﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ContentController;
using UnityEngine.SceneManagement;

public class ShopController : MonoBehaviour
{
    public GameObject shopCardPrefab;
    public GameObject awardPanelPrefab;
    public Sprite iconCoin;
    public Sprite iconHeart;

    public GameObject buyPanel;
    public GameObject NoBuyPnl;

    private List<GameObject> cardShopList;
    private int clickedCardIndex;

    void Start()
    {
        ContentController contentController = GameObject.FindGameObjectWithTag("ContentController").GetComponent<ContentController>();
        contentController.LoadContent();
        contentController.UpdatePurchasedItemList();

        SetupHeader();
        SetupCards();
    }

    public void LoadScene(string _sceneName) {
        SceneManager.LoadScene(_sceneName);
    }

    public void CardClicked(CardShopBehaviour cardShopBehaviour) {
        clickedCardIndex = cardShopBehaviour.GetIndex();

        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();

        if (cardShopBehaviour.GetContent().type == "coins") {
            //Open ads
            GameObject.FindGameObjectWithTag("AdsController").GetComponent<AdsController>().ShowRewardedAd(CardClickedBuy);
        } else {
            if (cardShopBehaviour.GetContent().price > gameDataController.gameData.coin) {
                NoBuyPnl.SetActive(true);
            } else {
                buyPanel.SetActive(true);
                GameObject.Find("PriceTxt").GetComponent<Text>().text = cardShopBehaviour.GetContent().price + "";
            }
        }
    }

    public void CardClickedBuy() {
        buyPanel.SetActive(false);

        Content content = cardShopList[clickedCardIndex].GetComponent<CardShopBehaviour>().GetContent();
        string awardTitle = "new "+content.type;
        Sprite awardSprite = content.type == "color" ? null : content.sprite;
        Color awardColor = content.type == "color" ? content.color : Color.white;

        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        PopUpController popUpController = GameObject.FindGameObjectWithTag("PopUpController").GetComponent<PopUpController>();

        GameObject objAward = popUpController.Add(awardPanelPrefab);
        objAward.GetComponent<PopUpAwardBehaviour>().Setup(awardTitle, awardSprite, awardColor, false, -1, null, null);

        if (content.type == "coins") {
            gameDataController.gameData.coin += 100;

            gameDataController.gameData.coin -= cardShopList[clickedCardIndex].GetComponent<CardShopBehaviour>().GetContent().price;
            gameDataController.SaveGameData();
        } else if (content.type == "heart") {
            gameDataController.gameData.heart++;

            gameDataController.gameData.coin -= cardShopList[clickedCardIndex].GetComponent<CardShopBehaviour>().GetContent().price;
            gameDataController.SaveGameData();
        } else {
            int itemIndex = clickedCardIndex - 2;
            if (!gameDataController.gameData.purchasedItemList.Contains(itemIndex)) {
                gameDataController.gameData.purchasedItemList.Add(itemIndex);

                gameDataController.gameData.coin -= cardShopList[clickedCardIndex].GetComponent<CardShopBehaviour>().GetContent().price;
                gameDataController.SaveGameData();

                cardShopList[clickedCardIndex].GetComponent<CardShopBehaviour>().Setup(this,
                    cardShopList[clickedCardIndex].GetComponent<CardShopBehaviour>().GetContent(),
                    clickedCardIndex);
            }
        }
    }

    private void SetupHeader() {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        ContentController contentController = GameObject.FindGameObjectWithTag("ContentController").GetComponent<ContentController>();

        string purchasedAmountTxt = (gameDataController.gameData.purchasedItemList.Count + contentController.GetFreeContentListCount()) +
            "/" + contentController.GetContentList().Count;
        GameObject.Find("PurchasedAmountTxt").GetComponent<Text>().text = purchasedAmountTxt;
    }

    private void SetupCards() {
        cardShopList = new List<GameObject>();

        CreateDefaultCards(cardShopList);
        CreateResourcesCards(cardShopList);
    }

    private void CreateDefaultCards(List<GameObject> cardShopList) {
        Content coinContent = new Content();
        coinContent.type = "coins";
        coinContent.sprite = iconCoin;
        coinContent.price = 0;

        GameObject coinCardObj = Instantiate(shopCardPrefab, Vector3.zero, Quaternion.identity);
        coinCardObj.GetComponent<CardShopBehaviour>().Setup(this, coinContent, cardShopList.Count);
        cardShopList.Add(coinCardObj);

        Content heartContent = new Content();
        heartContent.type = "heart";
        heartContent.sprite = iconHeart;
        heartContent.price = 100;
        GameObject heartCardObj = Instantiate(shopCardPrefab, Vector3.zero, Quaternion.identity);
        heartCardObj.GetComponent<CardShopBehaviour>().Setup(this, heartContent, cardShopList.Count);
        cardShopList.Add(heartCardObj);
    }

    private void CreateResourcesCards(List<GameObject> cardShopList) {
        ContentController contentController = GameObject.FindGameObjectWithTag("ContentController").GetComponent<ContentController>();

        List<Content> contentList = contentController.GetContentList();
        for (int i = 0; i < contentList.Count; i++) {
            GameObject obj = Instantiate(shopCardPrefab, Vector3.zero, Quaternion.identity);
            obj.GetComponent<CardShopBehaviour>().Setup(this, contentList[i], cardShopList.Count);
            cardShopList.Add(obj);
        }
    }
}

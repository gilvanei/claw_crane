﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBehaviour
{
    private GamePlayController gamePlayController;
    private int streak = 0;


    public ScoreBehaviour(GamePlayController _gamePlayController) {
        gamePlayController = _gamePlayController;
    
        SetScoreReset();
    }

    public void SetLevelUp() {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        gameDataController.gameData.level++;
        gamePlayController.GameOver(true);
    }

    public void SetStreakPlus() {
        streak ++;
    }

    public void SetStreakReset() {
        streak = 0;
    }

    public void SetMeCountPlus() {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        gameDataController.gamePlayData.meCount++;
        gameDataController.gamePlayData.hitToWin--;
        if(gameDataController.gamePlayData.hitToWin <= 0) {
            SetLevelUp();
        }
    }

    public void SetScoreReset() {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        gameDataController.gamePlayData.score = (0);
        gamePlayController.scorePanelController.Set();
    }

    public void SetScorePlus(Vector3 position) {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        int scoreNew = gameDataController.gamePlayData.score + (streak);
        gameDataController.gamePlayData.score = (scoreNew);

        string scoreGainText = "+" + streak;
        gamePlayController.CreatePointsPopup(scoreGainText, position, Color.green);

        gamePlayController.scorePanelController.Set();
    }
}

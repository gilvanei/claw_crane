﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupBehaviour
{
    private bool enable;

    private GamePlayController gamePlayController;

    private float setupTedTime;
    private List<string> listUniqueId;

    public SetupBehaviour(GamePlayController _gamePlayController)
    {
        gamePlayController = _gamePlayController;

        SetEnable(false);
    }

    public void Update() {
        if (!enable) {
            return;
        }

        // end of setup phase
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        bool endCondition = enable & gamePlayController.meController.GetMeListSize() >= gameDataController.gamePlayData.meAmount;
        if (endCondition) {
            enable = false;
            gamePlayController.shuffleBehaviour.SetEnable(false);
        }

        //  Create me's with a delay
        setupTedTime -= Time.deltaTime;
        if (setupTedTime < 0) {
            gamePlayController.meController.CreateMe(listUniqueId[0]);
            listUniqueId.RemoveAt(0);

            setupTedTime = Random.Range(0.1f, 0.2f);
        }
    }

    public void SetEnable(bool _enable) {
        enable = _enable;
        if (enable) {
            setupTedTime = 0f;
            listUniqueId = CreateListWithUniqueId();
        }
    }

    public bool GetEnable() {
        return enable;
    }

    private List<string> CreateListWithUniqueId() {
        List<string> list = new List<string>();

        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        for (int i=0; i < gameDataController.gamePlayData.meAmount; i++) {
            string uniqueId = gamePlayController.meController.GetUniqueId();
            list.Add(uniqueId);
        }
        list = ShuffleList(list);

        return list;
    }

    private List<string> ShuffleList(List<string> list) {;
        int n = list.Count;

        while (n > 1) {
            n--;
            int k = Random.Range(0, n + 1);
            string value = list[k];
            list[k] = list[n];
            list[n] = value;
        }

        return list;
    }
}

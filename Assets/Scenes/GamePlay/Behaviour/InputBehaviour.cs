﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputBehaviour
{
    public bool enable;

    private GamePlayController gamePlayController;

    private bool isMobile = (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer);
    private Vector3 clickPosition;

    public InputBehaviour(GamePlayController _gamePlayController) {
        enable = false;

        gamePlayController = _gamePlayController;
    }

    public void Update() {
        if (!enable) {
            return;
        }

        if (gamePlayController.shuffleBehaviour.IsEnable()) {
            return;
        }

        GameObject clickedObject = GetClick();
        if (clickedObject != null) {
            GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
            if (clickedObject.GetComponent<MeBehaviour>().id == gameDataController.gamePlayData.correctMe) {
                SelectCorrectTed(clickedObject);
            } else {
                SelectWrongTed();
            }
        }
    }

    private void SelectCorrectTed(GameObject gameObject) {
        AudioController audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
        audioController.PlayAudioSfx(audioController.audioLibrary.meRight);

        gamePlayController.meController.RemoveMe(gameObject);
        gamePlayController.meController.CreateMe();

        gamePlayController.scoreBehaviour.SetStreakPlus();
        gamePlayController.scoreBehaviour.SetScorePlus(new Vector3(clickPosition.x, clickPosition.y, 0f));
        gamePlayController.scoreBehaviour.SetMeCountPlus();
        gamePlayController.shuffleBehaviour.SetEnable(true);

        gamePlayController.clockBehaviour.ResetRoundTime();
    }

    private void SelectWrongTed() {
        AudioController audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
        audioController.PlayAudioSfx(audioController.audioLibrary.meWrong);

        gamePlayController.scoreBehaviour.SetStreakReset();
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        gamePlayController.clockBehaviour.SetMinusTime(gameDataController.gamePlayData.minusTimeWhenWrong, 
            new Vector3(clickPosition.x, clickPosition.y, 0f));
        gamePlayController.StartCoroutine(CameraShake.Instance.Shake(.15f, .2f));
    }

    private GameObject GetClick() {
        if (isMobile) {
            if ((Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)) {
                return GetClickEnter();
            }
        } else {
            if (Input.GetButtonDown("Fire1")) {
                return GetClickEnter();
            }
        }

        return null;
    }

    private GameObject GetClickEnter() {
        clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        RaycastHit2D hit = Physics2D.Raycast(clickPosition, Vector2.zero);
        if (hit.collider != null) {
            return hit.collider.gameObject;
        } else {
            return null;
        }
    }
}

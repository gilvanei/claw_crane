﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShuffleBehaviour
{
    private bool enable;

    private GamePlayController gamePlayController;

    private float shuffleCountdown;
    private const float shuffleCountdownDefault = 1;

    private float shuffleStepCountdown;
    private const float shuffleStepCountdownDefault = 0.05f;

    public ShuffleBehaviour(GamePlayController _gamePlayController) {
        enable = false;

        gamePlayController = _gamePlayController;

        shuffleCountdown = shuffleCountdownDefault;
        shuffleStepCountdown = shuffleStepCountdownDefault;
    }

    public void Update() {
        if (!enable) {
            return;
        }

        shuffleCountdown -= Time.deltaTime;

        shuffleStepCountdown -= Time.deltaTime;
        if (shuffleStepCountdown < 0) {
            shuffleStepCountdown = shuffleStepCountdownDefault;
            SelectTedAnswer();
        }

        if (shuffleCountdown < 0) {
            SetEnable(false);
        }
    }

    public void SetEnable(bool _enable) {
        enable = _enable;
        if (enable) {
            AudioController audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
            audioController.PlayAudioSfx(audioController.audioLibrary.meShuffle);
            shuffleCountdown = shuffleCountdownDefault;
            shuffleStepCountdown = shuffleStepCountdownDefault;
        } else {
            SelectTedAnswer();
        }
    }

    public bool IsEnable() {
        return enable;
    }

    public void SelectTedAnswer() {
        ContentController contentController = GameObject.FindGameObjectWithTag("ContentController").GetComponent<ContentController>();
        MeUIAnswer uiTedAnswer = new MeUIAnswer();
        GameObject gameObject = gamePlayController.meController.GetRandomMe();

        //5 Partes + body + color
        List<int> listIndex = new List<int>();
        listIndex.Add(GetRandomUniqueIndexPart(listIndex));
        uiTedAnswer.partOneText = contentController.GetResourceName(listIndex[0]);
        uiTedAnswer.partOneImage = GetUIPartSprite(listIndex[0], gameObject);

        listIndex.Add(GetRandomUniqueIndexPart(listIndex));
        uiTedAnswer.partTwoText = contentController.GetResourceName(listIndex[1]);
        uiTedAnswer.partTwoImage = GetUIPartSprite(listIndex[1], gameObject);

        listIndex.Add(GetRandomUniqueIndexPart(listIndex));
        uiTedAnswer.partThreeText = contentController.GetResourceName(listIndex[2]);
        uiTedAnswer.partThreeImage = GetUIPartSprite(listIndex[2], gameObject);

        GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>().gamePlayData.correctMe = (gameObject.GetComponent<MeBehaviour>().id);
        gamePlayController.tipsPanelController.SetTipsUpdate(uiTedAnswer);
    }

    private int GetRandomUniqueIndexPart(List<int> listIndex) {
        int indexPart = Random.Range(1, 6);
        while (listIndex.Contains(indexPart)) {
            indexPart = Random.Range(1, 6);
        }

        return indexPart;
    }

    private SpriteRenderer GetUIPartSprite(int indexPart, GameObject gameobject) {
        SpriteRenderer result = gameobject.GetComponent<SpriteRenderer>();

        if (indexPart >= 2) {
            result = gameobject.transform.GetChild(indexPart - 2).GetComponent<SpriteRenderer>();
        }

        if (result.sprite == null) {
            result.color = new Color(0, 0, 0, 0);
        }

        return result;
    }
}

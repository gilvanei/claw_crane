﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockBehaviour
{
    public bool enable;

    private float currentRoundTime;

    private GamePlayController gamePlayController;

    public ClockBehaviour(GamePlayController _gamePlayController)
    {
        gamePlayController = _gamePlayController;

        enable = false;

        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        gameDataController.gamePlayData.gamePlayTime = 0;
        ResetRoundTime();
    }

    public void Update() {
        if (!enable) {
            return;
        }

        // Update gameplay variable
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        gameDataController.gamePlayData.gamePlayTime += Time.deltaTime;
        gamePlayController.scorePanelController.Set();

        if (!gamePlayController.shuffleBehaviour.IsEnable()) {
            currentRoundTime -= Time.deltaTime;

            // GameOver condition
            if (currentRoundTime < 0) {
                gamePlayController.AskContinue();
            }
        }
    }


    public float GetCurrentRoundTime() {
        return currentRoundTime;
    }

    public void SetMinusTime(float _minusTime, Vector3 position) {
        currentRoundTime -= _minusTime;

        string text = "NOT ME \n-"+_minusTime+" sec";
        gamePlayController.CreatePointsPopup(text, position, Color.red);
        gamePlayController.scorePanelController.Set();
    }

    public void ResetRoundTime() {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        currentRoundTime = gameDataController.gamePlayData.roundTime;
    }
}

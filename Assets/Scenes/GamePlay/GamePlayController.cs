﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GamePlayController : MonoBehaviour
{
    //  Dependencies
    public GameObject popUpAwardPrefab;
    public GameObject popUpTutorialPrefab;
    public GameObject popUpContinuePrefab;
    public Sprite coinSprite;
    public Sprite giftSprite;

    public ScorePanelController scorePanelController;
    public TipsPanelController tipsPanelController;

    //  Prefabs inputs
    public PointsPopup pointsPopupPrefab;
    public GameObject mePrefab;

    //  Childrens
    public MeController meController;
    public InputBehaviour inputBehaviour;
    public ShuffleBehaviour shuffleBehaviour;
    public ScoreBehaviour scoreBehaviour;
    public ClockBehaviour clockBehaviour;
    public SetupBehaviour setupBehaviour;

    private bool paused;
    private bool started;
    private GameObject objTutorial;

    void Start() {
        ContentController contentController = GameObject.FindGameObjectWithTag("ContentController").GetComponent<ContentController>();
        contentController.LoadContent();
        contentController.UpdatePurchasedItemList();

        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        gameDataController.CreateGamePlayData();
 
        meController = new MeController(this);
        shuffleBehaviour = new ShuffleBehaviour(this);
        
        inputBehaviour = new InputBehaviour(this);
        setupBehaviour = new SetupBehaviour(this);
        clockBehaviour = new ClockBehaviour(this);
        scoreBehaviour = new ScoreBehaviour(this);

        AudioController audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent <AudioController>();
        audioController.PlayAudioSound(audioController.audioLibrary.musicGamePlay);
        scorePanelController.Set();
        paused = true;
        started = false;
    }

    void Update() {
        if (started == false) {
            if (objTutorial == null) {
                PopUpController popUpController = GameObject.FindGameObjectWithTag("PopUpController").GetComponent<PopUpController>();
                objTutorial = popUpController.Add(popUpTutorialPrefab);
                objTutorial.GetComponent<PopUpTutorial>().SetGamePlayController(this);
                paused = true;
            }
        }

        if (paused) {
            return;
        }

        setupBehaviour.Update();
        clockBehaviour.Update();
        shuffleBehaviour.Update();
        inputBehaviour.Update();
    }

    public void GameOver(bool win) {
        AudioController audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        PopUpController popUpController = GameObject.FindGameObjectWithTag("PopUpController").GetComponent<PopUpController>();
        ContentController contentController = GameObject.FindGameObjectWithTag("ContentController").GetComponent<ContentController>();
        if (win) {
            audioController.PlayAudioSfx(audioController.audioLibrary.levelUp);
        }
        int earnCoins = gameDataController.gamePlayData.score;
        gameDataController.gameData.coin += earnCoins;

        paused = true;
        string awardTitle = win ? "Congrats you earned" : "Better luck next time";
        awardTitle += " "+earnCoins + " coins";
        Sprite awardSprite = coinSprite;
        Color awardColor = Color.white;
        string closeScene = win ? null : "MainMenuScene";

        GameObject objCoinAward = popUpController.Add(popUpAwardPrefab);
        objCoinAward.GetComponent<PopUpAwardBehaviour>().Setup(awardTitle, awardSprite, awardColor, false, -1, null, closeScene);

        if (win) {
            string awardItemTitle ="watch the ad to unlock a new item";
            Sprite awardItemSprite = giftSprite;
            Color awardItemColor = Color.white;
            string itemCloseScene = "MainMenuScene";
            int awardIndex = contentController.GetRandomUnpurchasedContent();

            GameObject objContentAward = popUpController.Add(popUpAwardPrefab);
            objContentAward.GetComponent<PopUpAwardBehaviour>().Setup(awardItemTitle, awardItemSprite, awardItemColor, 
                true, awardIndex, popUpAwardPrefab, itemCloseScene);
        }

        gameDataController.SaveGameData();
    }

    public void AskContinue() {
        AudioController audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        PopUpController popUpController = GameObject.FindGameObjectWithTag("PopUpController").GetComponent<PopUpController>();
        audioController.PlayAudioSfx(audioController.audioLibrary.gameOver);
        paused = true;

        GameObject popUpContinue = popUpController.Add(popUpContinuePrefab);
        popUpContinue.GetComponent<PopUpContinue>().Setup(this);

        gameDataController.SaveGameData();
    }

    public void SetContinue() {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        paused = false;
        clockBehaviour.ResetRoundTime();
        shuffleBehaviour.SetEnable(true);

        gameDataController.gameData.heart--;
        gameDataController.SaveGameData();
    }

    public void SetStart() {
        started = true;
        paused = false;
        inputBehaviour.enable = true;
        setupBehaviour.SetEnable(true);
        clockBehaviour.enable = true;
        shuffleBehaviour.SetEnable(true);
    }

    public void OpenPause() {
        SetObjectActive("PanelPause", true);
        paused = true;
    }

    public void ClosePause() {
        SetObjectActive("PanelPause", false);
        paused = false;
    }

    public void LoadScene(string _sceneName) {
        SceneManager.LoadScene(_sceneName);
    }

    public void CreatePointsPopup(string scoreValue, Vector3 position, Color color) {
        PointsPopup pointsPopup = Instantiate(pointsPopupPrefab, position, Quaternion.identity);
        pointsPopup.Setup(scoreValue, color);
    }

    public GameObject CreateMeObject() {
        GameObject obj = Instantiate(mePrefab, new Vector3(Random.Range(-1.8f, 1.8f), 3.2f, 0), Quaternion.identity);
        obj.transform.SetParent(this.transform);
        return obj;
    }

    private void SetObjectActive(string objectName, bool newIsActive) {
        GameObject obj = FindObjectEvenIfInactive(objectName);
        obj.SetActive(newIsActive);
    }

    private GameObject FindObjectEvenIfInactive(string name) {
        GameObject parent = GameObject.Find("Canvas");
        Transform[] trs = parent.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in trs) {
            if (t.name == name) {
                return t.gameObject;
            }
        }
        return null;
    }
}

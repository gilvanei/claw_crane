﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public Toggle toggleSound;
    public Toggle toggleSFX;


    void Start() {
        ContentController contentController = GameObject.FindGameObjectWithTag("ContentController").GetComponent<ContentController>();
        contentController.LoadContent();
        contentController.UpdatePurchasedItemList();

        //Setup Game Data
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        gameDataController.CreateGamePlayData();

        // Setup Audio
        AudioController audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
        toggleSound.SetIsOnWithoutNotify(audioController.isSoundActive);
        toggleSFX.SetIsOnWithoutNotify(audioController.isSfxActive);
        audioController.PlayAudioSound(audioController.audioLibrary.musicMenu);

        UpdateLevelUI();
    }

    public void LoadScene(string _sceneName) {
        SceneManager.LoadScene(_sceneName);
    }

    public void ToggleSound() {
        AudioController audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();

        audioController.ToggleSound();
        gameDataController.SaveGameData();
    }

    public void ToggleSFX() {
        AudioController audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();

        audioController.ToggleSFX();
        gameDataController.SaveGameData();
    }

    private void UpdateLevelUI() {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();

        GameObject.Find("LevelTxt").GetComponent<Text>().text = "Level " + gameDataController.gameData.level;
        GameObject.Find("HitToWinTxt").GetComponent<Text>().text = "" + gameDataController.gamePlayData.hitToWin;
        GameObject.Find("MeAmountTxt").GetComponent<Text>().text = "" + gameDataController.gamePlayData.meAmount;
        GameObject.Find("RoundTimeTxt").GetComponent<Text>().text = "" + gameDataController.gamePlayData.roundTime;
        GameObject.Find("MinusTimeWhenWrongTxt").GetComponent<Text>().text = "- " + gameDataController.gamePlayData.minusTimeWhenWrong;
    }
}

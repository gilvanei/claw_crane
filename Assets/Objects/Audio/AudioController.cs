﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioSource audioSourceSound;
    public AudioSource audioSourceSfx;
    public AudioLibrary audioLibrary;

    public bool isSoundActive;
    public bool isSfxActive;

    void Awake() {
        if(GameObject.FindGameObjectsWithTag("AudioController").Length > 1) {
            Destroy(this.gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    void Start() {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();

        isSoundActive = gameDataController.gameData.sound;
        isSfxActive = gameDataController.gameData.sfx;
        audioSourceSound.mute = !isSoundActive;
        audioSourceSfx.mute = !isSfxActive;
    }

    public void PlayAudioSfx(AudioClip audioClip) {
        audioSourceSfx.PlayOneShot(audioClip);
    }

    public void PlayAudioSound(AudioClip audioClip) {
        if (audioSourceSound.clip == audioClip) {
            return;
        }

        audioSourceSound.Stop();
        audioSourceSound.clip = audioClip;
        audioSourceSound.Play();
    }

    public void ToggleSound() {
        isSoundActive = !isSoundActive;
        audioSourceSound.mute = !isSoundActive;
    }

    public void ToggleSFX() {
        isSfxActive = !isSfxActive;
        audioSourceSfx.mute = !isSfxActive;
    }
}

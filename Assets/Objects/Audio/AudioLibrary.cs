using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioLibrary: MonoBehaviour {

    public AudioClip musicMenu;
    public AudioClip musicGamePlay;
    public AudioClip buttonClick;
    public AudioClip meShuffle;
    public AudioClip meWrong;
    public AudioClip meRight;
    public AudioClip levelUp;
    public AudioClip gameOver;
};
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeUIAnswer
{

    public SpriteRenderer partOneImage;
    public SpriteRenderer partTwoImage;
    public SpriteRenderer partThreeImage;
    public string partOneText;
    public string partTwoText;
    public string partThreeText;
}

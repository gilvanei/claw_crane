﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeController
{

    private GamePlayController gamePlayController;

    private List<GameObject> meList = new List<GameObject>();
    private List<string> meIdList = new List<string>();

    public MeController(GamePlayController _gamePlayController) {
        gamePlayController = _gamePlayController;
    }

    public void CreateMe() {
        string uniqueId = GetUniqueId();
        CreateMe(uniqueId);
    }


    public string GetUniqueId() {
        string uniqueId;
        do {
            uniqueId = GameObject.FindGameObjectWithTag("ContentController").GetComponent<ContentController>().GenerateUniqueId();
        } while (meIdList.Contains(uniqueId));

        return uniqueId;
    }

    public GameObject CreateMe(string uniqueId) {
        GameObject obj = gamePlayController.CreateMeObject();
        obj.GetComponent<MeBehaviour>().SetParts(uniqueId);

        meList.Add(obj);
        meIdList.Add(obj.GetComponent<MeBehaviour>().id);
        return obj;
    }

    public void RemoveMe(GameObject gameObject) {
        meList.Remove(gameObject);
        meIdList.Remove(gameObject.GetComponent<MeBehaviour>().id);
        gameObject.GetComponent<MeBehaviour>().SetDestroy();
    }

    public int GetMeListSize() {
        return meList.Count;
    }

    public GameObject GetRandomMe() {
        return meList[Random.Range(0, meList.Count)];
    }
}

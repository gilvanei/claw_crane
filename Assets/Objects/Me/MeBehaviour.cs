﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeBehaviour : MonoBehaviour
{
    public string id;
    public GameObject meExplosionParticleSystemPrefab;

    private int colorId;
    private int bodyId;
    private int hatId;
    private int eyeId;
    private int earId;
    private int noseId;
    private int mouthId;

    private ContentController contentController;

    public void SetParts(string _id) {
        contentController = GameObject.FindGameObjectWithTag("ContentController").GetComponent<ContentController>();

        this.id = _id;

        string[] split = _id.Split(char.Parse("|"));
        SetColor(int.Parse(split[0]));
        SetBody(int.Parse(split[1]));
        SetHat(int.Parse(split[2]));
        SetEye(int.Parse(split[3]));
        SetEar(int.Parse(split[4]));
        SetNose(int.Parse(split[5]));
        SetMouth(int.Parse(split[6]));
    }

    public void SetDestroy() {
        GameObject obj = Instantiate(meExplosionParticleSystemPrefab, this.gameObject.transform.position, Quaternion.identity);

        var ps = obj.GetComponent<ParticleSystem>();
        var main = ps.main;
        main.startColor = contentController.GetColorList()[colorId];

        Destroy(this.gameObject);
    }

    private void SetColor(int _colorId) {
        this.colorId = _colorId;
        this.gameObject.GetComponent<SpriteRenderer>().color = contentController.GetColorList()[colorId];
    }

    private void SetBody(int _bodyId) {
        this.bodyId = _bodyId;
        this.gameObject.GetComponent<SpriteRenderer>().sprite = contentController.GetBodyList()[bodyId];
    }

    private void SetHat(int _hatId) {
        this.hatId = _hatId;
        this.gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = contentController.GetHatList()[hatId];
        this.gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.white;
    }

    private void SetEye(int _eyeId) {
        this.eyeId = _eyeId;
        this.gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = contentController.GetEyeList()[eyeId];
        this.gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().color = Color.white;
    }

    private void SetEar(int _earId) {
        this.earId = _earId;
        this.gameObject.transform.GetChild(2).GetComponent<SpriteRenderer>().sprite = contentController.GetEarList()[earId];
        this.gameObject.transform.GetChild(2).GetComponent<SpriteRenderer>().color = contentController.GetColorList()[colorId];
    }

    private void SetNose(int _noseId) {
        this.noseId = _noseId;
        this.gameObject.transform.GetChild(3).GetComponent<SpriteRenderer>().sprite = contentController.GetNoseList()[noseId];
        this.gameObject.transform.GetChild(3).GetComponent<SpriteRenderer>().color = Color.white;
    }

    private void SetMouth(int _mouthId) {
        this.mouthId = _mouthId;
        this.gameObject.transform.GetChild(4).GetComponent<SpriteRenderer>().sprite = contentController.GetMouthList()[mouthId];
        this.gameObject.transform.GetChild(4).GetComponent<SpriteRenderer>().color = Color.white;
    }
}

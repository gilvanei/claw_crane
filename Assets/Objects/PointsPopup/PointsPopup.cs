﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PointsPopup : MonoBehaviour
{
    private const float DISAPPEAR_TIMER_MAX = 0.5f;

    private Color textColor;
    private float disappearTimer;
    private float disappearSpeed;

    public void Setup(string pointsValue, Color _textColor) {
        TextMeshPro textMesh = transform.GetComponent<TextMeshPro>();
        textColor = _textColor;
        textMesh.color = textColor;

        textMesh.SetText(pointsValue.ToString());

        disappearTimer = DISAPPEAR_TIMER_MAX;
    }

    void Update() {
        float moveYSpeed = 5f;
        transform.position += new Vector3(0, moveYSpeed) * Time.deltaTime;

        if(disappearTimer > DISAPPEAR_TIMER_MAX * 0.5f) {
            // First half of the popup lifetime
            float increaseScaleAmount = 1f;
            transform.localScale += Vector3.one * increaseScaleAmount * Time.deltaTime;
        } else {
            // Secound half of the popup lifetime
            float decreaseScaleAmount = 1f;
            transform.localScale -= Vector3.one * decreaseScaleAmount * Time.deltaTime;

        }

        disappearTimer -= Time.deltaTime;
        if (disappearTimer < 0) {
            disappearSpeed = 3f;
            textColor.a -= disappearSpeed * Time.deltaTime;
            TextMeshPro textMesh = transform.GetComponent<TextMeshPro>();
            textMesh.color = textColor;

            if (textColor.a < 0) {
                Destroy(this.gameObject);
            }
        }
    }


}

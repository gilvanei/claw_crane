using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScorePanelController : MonoBehaviour
{
    public Text timerText;
    public Text levelText;
    public Text scoreText;

    public string gamePlayTime;
    public string level;
    public string score;

    public void Set() {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        timerText.text = FormatTime(gameDataController.gamePlayData.gamePlayTime);
        timerText.color = Color.white;

        levelText.text = "" + gameDataController.gameData.level;
        levelText.color = Color.white;

        scoreText.text = "" + gameDataController.gamePlayData.score;
        scoreText.color = Color.white;

        gamePlayTime = ""+ gameDataController.gamePlayData.gamePlayTime;
        level = ""+ gameDataController.gameData.level;
        score = ""+ gameDataController.gamePlayData.score;
    }


    private string FormatTime(float timer) {
        float minutes = Mathf.Floor(timer / 60);
        float seconds = timer % 60;
        return (minutes).ToString("00") + ":" + (Mathf.RoundToInt(seconds)).ToString("00");
    }
}

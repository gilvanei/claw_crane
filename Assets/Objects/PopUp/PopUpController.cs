using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpController : MonoBehaviour
{
    private List<GameObject> popUpList;
    private GameObject popUpOnScreen;

    void Awake() {
        if (GameObject.FindGameObjectsWithTag("PopUpController").Length > 1) {
            Destroy(this.gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    void Start() {
        popUpList = new List<GameObject>();
    }

    public void Update() {
        if (popUpList.Count > 0 && popUpOnScreen == null) {
            SetOnScreen();
        }
    }

    public GameObject Add(GameObject popUpPrefab) {
        GameObject popUp = Instantiate(popUpPrefab, Vector3.zero, Quaternion.identity);

        //Set as inactive
        popUp.SetActive(false);

        //Set Canvas as parent
        GameObject pageParent = GameObject.Find("Canvas");
        popUp.transform.SetParent(pageParent.transform);

        //Set anchor, scale and position
        RectTransform rt = popUp.GetComponent<RectTransform>();
        rt.anchorMin = Vector2.zero;
        rt.anchorMax = Vector2.one;
        rt.pivot = new Vector2(0.5f, 0.5f);
        rt.localScale = Vector3.one;
        rt.localPosition = Vector2.one;
        rt.offsetMin = new Vector2(0f, 0f);
        rt.offsetMax = new Vector2(0f, 0f);

        popUpList.Add(popUp);
        return popUp;
    }

    private void SetOnScreen() {
        popUpOnScreen = popUpList[0];
        popUpOnScreen.SetActive(true);

        popUpList.RemoveAt(0);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpTutorial : MonoBehaviour
{
    private GamePlayController gamePlayController;

    public void Close() {
        Destroy(this.gameObject);
        gamePlayController.SetStart();
    }

    public void SetGamePlayController(GamePlayController _gamePlayController) {
        gamePlayController = _gamePlayController;
    }
}

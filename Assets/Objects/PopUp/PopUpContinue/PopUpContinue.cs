using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpContinue : MonoBehaviour
{
    private GamePlayController gamePlayController;

    public void Setup(GamePlayController _gamePlayController) {
        gamePlayController = _gamePlayController;

        GameObject objContinueResumeBtn = GetChildWithName(transform, "ContinueResumeBtn");
        GameObject objContinueShopBtn = GetChildWithName(transform, "ContinueShopBtn");
        GameObject objLifeNumberText = GetChildWithName(transform, "LifeNumberText");
        GameObject objLifeMinusText = GetChildWithName(transform, "LifeMinusText");
        GameObject objLifeIcon = GetChildWithName(transform, "LifeIcon");

        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        if (gameDataController.gameData.heart > 0) {
            objContinueResumeBtn.SetActive(true);
            objContinueShopBtn.SetActive(false);

            objLifeNumberText.GetComponent<Text>().text = "Lifes " + gameDataController.gameData.heart;
            objLifeNumberText.GetComponent<Text>().color = Color.white;
        } else {
            objContinueResumeBtn.SetActive(false);
            objContinueShopBtn.SetActive(true);

            objLifeMinusText.SetActive(false);
            objLifeNumberText.GetComponent<Text>().text = "No Life";
            objLifeNumberText.GetComponent<Text>().color = Color.white;

            objLifeIcon.GetComponent<Image>().color = new Color(0, 0, 0, 0.5f);
        }
    }

    public void SetResume() {
        Destroy(this.gameObject);
        gamePlayController.SetContinue();


    }

    public void SetGameOver() {
        Destroy(this.gameObject);
        gamePlayController.GameOver(false);
    }

    public void SetShop() {
        gamePlayController.LoadScene("ShopScene");
    }

    private GameObject GetChildWithName(Transform trans, string name) {
        Transform childTrans = trans.Find(name);
        if (childTrans != null) {
            return childTrans.gameObject;
        } else {
            return null;
        }
    }
}

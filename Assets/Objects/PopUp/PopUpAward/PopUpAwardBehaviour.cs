using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using static ContentController;

public class PopUpAwardBehaviour : MonoBehaviour
{
    private string closeScene;
    private int contentId;
    private GameObject popUpPrefab;

    public void Close() {
        if(closeScene != null) {
            SceneManager.LoadScene(closeScene);
        }
        Destroy(this.gameObject);
    }

    public void Claim() {
        GameObject.FindGameObjectWithTag("AdsController").GetComponent<AdsController>().ShowRewardedAd(ReceiveAward);
    }

    private void ReceiveAward() {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        if (!gameDataController.gameData.purchasedItemList.Contains(contentId)) {
            gameDataController.gameData.purchasedItemList.Add(contentId);
        }

        Content content = GameObject.FindGameObjectWithTag("ContentController").GetComponent<ContentController>().GetContentList()[contentId];
        string awardTitle = "new " + content.type;
        Sprite awardSprite = content.type == "color" ? null : content.sprite;
        Color awardColor = content.type == "color" ? content.color : Color.white;

        PopUpController popUpController = GameObject.FindGameObjectWithTag("PopUpController").GetComponent<PopUpController>();
        GameObject objAward = popUpController.Add(popUpPrefab);
        objAward.GetComponent<PopUpAwardBehaviour>().Setup(awardTitle, awardSprite, awardColor, false, -1, null, "MainMenuScene");
        closeScene = null;
        Close();
    }

    public void Setup(string title, Sprite contentSprite, Color contentColor, bool showClaimEarnings, int contentId, GameObject popUpPrefab, string closeScene) {
        this.closeScene = closeScene;
        this.contentId = contentId;
        this.popUpPrefab = popUpPrefab;

        GetChildWithName(this.transform, "Title").GetComponent<Text>().text = title;

        GameObject objContent = GetChildWithName(this.transform, "Content");
        objContent.GetComponent<Image>().sprite = contentSprite;
        objContent.GetComponent<Image>().color = contentColor;

        GameObject objContentBackground = GetChildWithName(this.transform, "ContentBackground");
        objContentBackground.GetComponent<Image>().sprite = contentSprite;
        objContentBackground.GetComponent<Image>().color = Color.black;

        GetChildWithName(this.transform, "BtnAds").SetActive(showClaimEarnings);
        GameObject objBtnBack = GetChildWithName(this.transform, "BtnBack");
        GameObject objBtnNoThanks = GetChildWithName(this.transform, "BtnBack");
        if (showClaimEarnings) {
            objBtnNoThanks.SetActive(true);
            objBtnBack.SetActive(false);
        } else {
            objBtnNoThanks.SetActive(false);
            objBtnBack.SetActive(true);
        }
    }

    private GameObject GetChildWithName(Transform trans, string name) {
        Transform childTrans = trans.Find(name);
        if (childTrans != null) {
            return childTrans.gameObject;
        } else {
            return null;
        }
    }
}

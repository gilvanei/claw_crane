﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{

    public bool sound = true;
    public bool sfx = true;

    public int level = 1;
    public int coin = 0;
    public int heart = 0;

    public List<int> purchasedItemList = new List<int>();
}

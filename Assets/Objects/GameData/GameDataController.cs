﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


public class GameDataController : MonoBehaviour
{
    public GameData gameData;
    public GamePlayData gamePlayData;

    void Awake() {
        if (GameObject.FindGameObjectsWithTag("GameDataController").Length > 1) {
            Destroy(this.gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);

        gameData = LoadGameData();
        CreateGamePlayData();
    }

    public GameData LoadGameData() {
        GameData _gameData;

        string filePath = Application.persistentDataPath + "/save";
        if (File.Exists(filePath)) {
            try {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(filePath, FileMode.Open);

                _gameData = formatter.Deserialize(stream) as GameData;
                stream.Close();
            } catch {
                _gameData = new GameData();
                Debug.LogWarning("Iniciando novo SaveGame");
            }
        } else {
            _gameData = new GameData();
            Debug.LogWarning("Iniciando novo SaveGame");
        }

        // Workaround FIXME
        if(_gameData.level == 0) {
            _gameData.level = 1;
        }
        if (_gameData.purchasedItemList == null) {
            _gameData.purchasedItemList = new List<int>();
        }

        return _gameData;
    }

    public void SaveGameData() {
        AudioController audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
        gameData.sound = audioController.isSoundActive;
        gameData.sfx = audioController.isSfxActive;

        BinaryFormatter formatter = new BinaryFormatter();
        string filePath = Application.persistentDataPath + "/save";
        FileStream stream = new FileStream(filePath, FileMode.Create);

        formatter.Serialize(stream, gameData);
        stream.Close();

        if (gameData.purchasedItemList == null) {
            gameData.purchasedItemList = new List<int>();
        }

        GameObject.FindGameObjectWithTag("ContentController").GetComponent<ContentController>().UpdatePurchasedItemList();
    }

    public void CreateGamePlayData() {
        gamePlayData =  new GamePlayData(gameData);
    }
}

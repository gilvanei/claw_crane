﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayData {

    public int hitToWin;
    public float meAmount;
    public float roundTime;
    public float minusTimeWhenWrong;

    public string correctMe;
    public int score;
    public int meCount;
    public float gamePlayTime;
    private int setupLevel;

    public GamePlayData(GameData _gameData) {
        correctMe = null;
        score = 0;
        meCount = 0;
        gamePlayTime = 0;

        int level = _gameData.level;

        if (setupLevel != level) {
            hitToWin = GetHitToWin(level);
            meAmount = GetMeAmount(level);
            roundTime = GetRoundTime(level);
            minusTimeWhenWrong = GetMinusTimeWhenWrong(level);
            setupLevel = level;
        }
    }

    private int GetHitToWin(int level) {
        int seed = 5;
        int leverage = level;

        return seed + leverage;
    }

    private int GetMeAmount(int level) {
        int seed = 5;
        int leverage = level / 2;

        return seed + leverage;
    }

    private int GetRoundTime(int level) {
        int seed = 10;
        int leverage = level / 10;
        int result = seed - leverage;
        if(result < 5) {
            result = 5;
        }

        return result;
    }

    private float GetMinusTimeWhenWrong(int level) {
        int seed = 1;
        float leverage = (level-1) / 20f;

        return seed + leverage;
    }
}

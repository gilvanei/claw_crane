﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelCoinHeartBehaviour : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        GameObject.Find("CoinText").GetComponent<Text>().text = gameDataController.gameData.coin + "";
        GameObject.Find("HeartText").GetComponent<Text>().text = gameDataController.gameData.heart + "";
    }
}

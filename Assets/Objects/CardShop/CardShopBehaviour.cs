﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ContentController;

public class CardShopBehaviour : MonoBehaviour
{
    private ShopController shopController;
    private Content content;
    public int index;

    public void Setup(ShopController _shopController, Content _content, int _index) {
        index = _index;
        content = _content;

        shopController = _shopController;
        SetContent(_content);
        SetParent();
        SetPosition(_index);
    }

    public void Clicked() {
        shopController.CardClicked(this);
    }

    public Content GetContent() {
        return content;
    }

    public int GetIndex() {
        return index;
    }

    private void SetContent(Content content) {
        if (content.type == "color") {
            GetChildWithName(this.transform, "Image").GetComponent<Image>().color = content.color;
        } else {
            if (content.content == "") {
                Destroy(GetChildWithName(this.transform, "Image").GetComponent<Image>());
            } else {
                GetChildWithName(this.transform, "Image").GetComponent<Image>().sprite = content.sprite;
            }
        }

        Color typeColor = GetTypeColor(content.type);

        GameObject typePnlObj = GetChildWithName(this.transform, "TypePnl");
        typePnlObj.GetComponent<Image>().color = typeColor;
        GetChildWithName(typePnlObj.transform, "TypeText").GetComponent<Text>().text = content.type;

        GameObject priceBtnObj = GetChildWithName(this.transform, "PriceBtn");
        if (content.purchased) {
            GetComponent<Image>().color = new Color(0.35f, 0.54f, 0.95f);
            Destroy(priceBtnObj);
            Destroy(this.GetComponent<Button>());
        } else {
            string priceText;
            if(content.type == "coin") {
                priceText = "GET";
            } else {
                priceText = content.price + "";
            }
            priceBtnObj.GetComponent<Image>().color = typeColor;
            GetChildWithName(priceBtnObj.transform, "PriceText").GetComponent<Text>().text = priceText;
        }

        if (content.type == "coin") {
            GameObject priceAdsImg = GetChildWithName(priceBtnObj.transform, "PriceAdsImg");
            priceAdsImg.GetComponent<Image>().color = Color.black;
            priceAdsImg.SetActive(true);
            GetChildWithName(priceBtnObj.transform, "PriceCoinImg").SetActive(false);
        } else {
            GetChildWithName(priceBtnObj.transform, "PriceAdsImg").SetActive(false);
            GetChildWithName(priceBtnObj.transform, "PriceCoinImg").SetActive(true);
        }

        GetChildWithName(this.transform, "ButtonAlert").SetActive(!content.purchased);
    }

    private Color GetTypeColor(string type) {
        if (type == "color") {
            return new Color(0.4f, 0.95f, 0.4f);
        } else if (type == "body") {
            return new Color(0.95f, 0.95f, 0.3f);
        } else if (type == "hat") {
            return new Color(0.95f, 0.4f, 0.3f);
        } else if (type == "eye") {
            return new Color(0.95f, 0.3f, 0.7f);
        } else if (type == "ear") {
            return new Color(0.6f, 0.3f, 0.95f);
        } else if (type == "nose") {
            return new Color(0.3f, 0.55f, 0.35f);
        } else if (type == "mouth") {
            return new Color(0.35f, 0.85f, 0.95f);
        } else {
            return Color.white;
        }
    }


    private void SetParent() {
        GameObject parentObj = GameObject.Find("CardsContainer");
        this.transform.SetParent(parentObj.transform);
    }

    private void SetPosition(int total) {
        RectTransform rt = this.GetComponent<RectTransform>();
        rt.localScale = Vector3.one;
        rt.anchorMin = new Vector2(0.5f, 1);
        rt.anchorMax = new Vector2(0.5f, 1);
        rt.pivot = new Vector2(0.5f, 1);

        int row = total / 4;
        int col = total % 4;
        float x = (col) * 24 - 36;
        float y = (row * -1) * 32 - 4;
        rt.localPosition = new Vector2(x, y);
    }

    private GameObject GetChildWithName(Transform trans, string name) {
        Transform childTrans = trans.Find(name);
        if (childTrans != null) {
            return childTrans.gameObject;
        } else {
            return null;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public GamePlayController gamePlayController;
    private Image image;

    private float percentage;

    void Start() {
        image = this.gameObject.transform.GetChild(0).GetComponent<Image>();
    }

    void Update()
    {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        percentage = gamePlayController.clockBehaviour.GetCurrentRoundTime() / gameDataController.gamePlayData.roundTime;
        image.fillAmount = percentage;
        image.color = CalcColor();
    }

    public Color GetColor() {
        return image.color;
    }

    private Color CalcColor() 
    {
        if(percentage > 0.66f) {
            return Color.green;
        }else if(percentage > 0.33f) {
            return Color.yellow;
        } else {
            return Color.red;
        }
    }
}

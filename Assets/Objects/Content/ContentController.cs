﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ContentController : MonoBehaviour
{

    private string jsonString;
    private ContentList jsonContentList;
    private List<Content> purchasedContentList;
    private List<Content> unpurchasedContentList;
    private List<Color> colorList;
    private List<Sprite> bodyList;
    private List<Sprite> hatList;
    private List<Sprite> eyeList;
    private List<Sprite> earList;
    private List<Sprite> noseList;
    private List<Sprite> mouthList;

    void Awake() {
        if (GameObject.FindGameObjectsWithTag("ContentController").Length > 1) {
            Destroy(this.gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public void LoadContent() {
        jsonContentList = LoadJson("ContentList.json");

        for (int i = 0; i < jsonContentList.content.Count; i++) {
            Content c = jsonContentList.content[i];

            //Sprites and Colors
            if (c.type == "color") {
                Color nColor = LoadColor(c);
                c.color = nColor;
            } else if (c.type == "body") {
                Sprite nSprite = LoadSprite("Sprites/body", c.content);
                c.sprite = nSprite;
            } else if (c.type == "hat") {
                Sprite nSprite = LoadSprite("Sprites/hat", c.content);
                c.sprite = nSprite;
            } else if (c.type == "eye") {
                Sprite nSprite = LoadSprite("Sprites/eye", c.content);
                c.sprite = nSprite;
            } else if (c.type == "ear") {
                Sprite nSprite = LoadSprite("Sprites/ear", c.content);
                c.sprite = nSprite;
            } else if (c.type == "nose") {
                Sprite nSprite = LoadSprite("Sprites/nose", c.content);
                c.sprite = nSprite;
            } else if (c.type == "mouth") {
                Sprite nSprite = LoadSprite("Sprites/mouth", c.content);
                c.sprite = nSprite;
            }
        }
    }

    public void UpdatePurchasedItemList() {
        purchasedContentList = new List<Content>();
        unpurchasedContentList = new List<Content>();
        colorList = new List<Color>();
        bodyList = new List<Sprite>();
        hatList = new List<Sprite>();
        eyeList = new List<Sprite>();
        earList = new List<Sprite>();
        noseList = new List<Sprite>();
        mouthList = new List<Sprite>();

        for (int i = 0; i < jsonContentList.content.Count; i++) {
            Content c = jsonContentList.content[i];

            bool isInPurchasedItemList = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>().gameData.purchasedItemList.Contains(c.id);
            bool isFree = c.price <= 0;
            c.purchased = isInPurchasedItemList | isFree;

            if (isInPurchasedItemList | isFree) {
                purchasedContentList.Add(c);

                if (c.type == "color") {
                    colorList.Add(c.color);
                } else if (c.type == "body") {
                    bodyList.Add(c.sprite);
                } else if (c.type == "hat") {
                    hatList.Add(c.sprite);
                } else if (c.type == "eye") {
                    eyeList.Add(c.sprite);
                } else if (c.type == "ear") {
                    earList.Add(c.sprite);
                } else if (c.type == "nose") {
                    noseList.Add(c.sprite);
                } else if (c.type == "mouth") {
                    mouthList.Add(c.sprite);
                }
            } else {
                unpurchasedContentList.Add(c);
            }
        }
    }

    public string GetResourceName(int index) {
        switch (index) {
            case 0:
                return "color";
            case 1:
                return "body";
            case 2:
                return "hat";
            case 3:
                return "eye";
            case 4:
                return "ear";
            case 5:
                return "nose";
            case 6:
                return "mouth";
            default:
                return "";
        }
    }

    public string GenerateUniqueId() {
        int colorId = Random.Range(0, colorList.Count);
        int bodyId = Random.Range(0, bodyList.Count);
        int hatId = Random.Range(0, hatList.Count);
        int eyeId = Random.Range(0, eyeList.Count);
        int earId = Random.Range(0, earList.Count);
        int noseId = Random.Range(0, noseList.Count);
        int mouthId = Random.Range(0, mouthList.Count);

        return string.Concat(colorId, "|", bodyId, "|", hatId, "|", eyeId, "|", earId, "|", noseId, "|", mouthId);
    }

    public List<Content> GetContentList() {
        return jsonContentList.content;
    }

    public int GetRandomUnpurchasedContent() {
        int randomIndex = (int)(Random.Range(0, unpurchasedContentList.Count));
        return unpurchasedContentList[randomIndex].id;
    }

    public int GetFreeContentListCount() {
        int count=0;
        for (int i=0; i<jsonContentList.content.Count; i++) {
            if (jsonContentList.content[i].price == 0) {
                count++;
            }
        }
        return count;
    }

    public List<Color> GetColorList() {
        return colorList;
    }

    public List<Sprite> GetBodyList() {
        return bodyList;
    }

    public List<Sprite> GetHatList() {
        return hatList;
    }

    public List<Sprite> GetEyeList() {
        return eyeList;
    }

    public List<Sprite> GetEarList() {
        return earList;
    }

    public List<Sprite> GetNoseList() {
        return noseList;
    }

    public List<Sprite> GetMouthList() {
        return mouthList;
    }


    private ContentList LoadJson(string filename) {
        string path = Path.Combine(Application.streamingAssetsPath + "/", filename);

        #if UNITY_EDITOR || UNITY_IOS
        jsonString = File.ReadAllText(path);

        #elif UNITY_ANDROID
        WWW reader = new WWW (path);
        while (!reader.isDone) {}

        jsonString = reader.text;
        #endif 
        
        return JsonUtility.FromJson<ContentList>(jsonString);
    }

    public Sprite LoadSprite (string folder, string fileName) {
        return Resources.Load<Sprite>(folder + "/" + fileName); ;
    }

    private Color LoadColor (Content content) {
        Color newCol;

        if (ColorUtility.TryParseHtmlString(content.content, out newCol)) {
            return newCol;
        }

        return Color.black;
    }

    [System.Serializable]
    public class Content
    {
        public int id;
        public string type;
        public string name;
        public string content;
        public int price;

        public Sprite sprite;
        public Color color;
        public bool purchased;
    }

    [System.Serializable]
    public class ContentList
    {
        public List<Content> content;

    }
}

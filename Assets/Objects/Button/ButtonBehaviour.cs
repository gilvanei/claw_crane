using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonBehaviour : MonoBehaviour
{

	void Start() {
		GetComponent<Button>().onClick.AddListener(TaskOnClick);
	}

	void TaskOnClick() {
		AudioController audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
		audioController.PlayAudioSfx(audioController.audioLibrary.buttonClick);
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsController : MonoBehaviour, IUnityAdsListener
{
#if UNITY_ANDROID 
    string gameId = "4388275";
    string interstitial = "Interstitial_Android";
    string rewarded = "Rewarded_Android";
    string banner = "Banner_Android";
#else
    string gameId = "4388274";
    string interstitial = "Interstitial_iOS";
    string rewarded = "Rewarded_iOS";
    string banner = "Banner_iOS";
#endif

    public delegate void DelegateMethod(); // This defines what type of method you're going to call.
    public DelegateMethod delegateMethodToCall; // This is the variable holding the method you're going to call.

    void Awake() {
        if (GameObject.FindGameObjectsWithTag("AdsController").Length > 1) {
            Destroy(this.gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        Advertisement.Initialize(gameId);
        Advertisement.AddListener(this);
        //ShowBanner();
    }

    public void ShowSkippableAd()
    {
        if (Advertisement.IsReady(interstitial)) {
            Advertisement.Show(interstitial);
        } else {
            Debug.Log("interstitial ad is not ready");
        }
    }

    public void ShowRewardedAd(DelegateMethod _delegateMethodToCall) {
        if (Advertisement.IsReady(rewarded)) {
            delegateMethodToCall = _delegateMethodToCall;
            Advertisement.Show(rewarded);
        } else {
            Debug.Log("rewarded ad is not ready");
        }
    }

    public void ShowBanner() {
        if (Advertisement.IsReady(banner)) {
            Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
            Advertisement.Banner.Show(banner);
        } else {
            StartCoroutine(RepeatShowBanner());
        }
    }

    public void HideBanner() {
        Advertisement.Banner.Hide();
    }

    IEnumerator RepeatShowBanner() {
        yield return new WaitForSeconds(0);
        ShowBanner();
    }

    public void OnUnityAdsReady(string placementId) {
        //Debug.Log("OnUnityAdsReady");
    }

    public void OnUnityAdsDidError(string message) {
        //Debug.Log("OnUnityAdsDidError");
    }

    public void OnUnityAdsDidStart(string placementId) {
        //Debug.Log("OnUnityAdsDidStart");
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult) {
        //Debug.Log("OnUnityAdsDidFinish");
        if (placementId == rewarded && showResult == ShowResult.Finished) {
            Debug.Log("Rewarded");
            delegateMethodToCall();
        }
    }
}

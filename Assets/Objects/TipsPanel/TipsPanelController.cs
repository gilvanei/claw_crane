using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TipsPanelController : MonoBehaviour
{

    public Image partOneImage;
    public Image partTwoImage;
    public Image partThreeImage;

    public Text partOneText;
    public Text partTwoText;
    public Text partThreeText;

    public Text hitToNextLevelText;

    public ProgressBar progressBar;

    public void SetTipsUpdate(MeUIAnswer uiTedAnswer) {
        partOneImage.sprite = uiTedAnswer.partOneImage.sprite;
        partOneImage.color = uiTedAnswer.partOneImage.color;
        partTwoImage.sprite = uiTedAnswer.partTwoImage.sprite;
        partTwoImage.color = uiTedAnswer.partTwoImage.color;
        partThreeImage.sprite = uiTedAnswer.partThreeImage.sprite;
        partThreeImage.color = uiTedAnswer.partThreeImage.color;

        partOneText.text = uiTedAnswer.partOneText;
        partTwoText.text = uiTedAnswer.partTwoText;
        partThreeText.text = uiTedAnswer.partThreeText;
    }

    void Update() {
        GameDataController gameDataController = GameObject.FindGameObjectWithTag("GameDataController").GetComponent<GameDataController>();
        hitToNextLevelText.text = "hit " + gameDataController.gamePlayData.hitToWin + " more";
        hitToNextLevelText.color = progressBar.GetColor();
    }
}
